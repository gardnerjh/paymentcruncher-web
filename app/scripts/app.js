'use strict';

/**
 * @ngdoc overview
 * @name paymentCruncher
 * @description
 * # paymentCruncher
 *
 * Main module of the application.
 */
angular
	.module('paymentCruncher', [
		'ngCookies',
		'ngRoute',
		'xml',
		'ui.bootstrap',
		'ui.sortable',
		'uiGmapgoogle-maps'
	])
	.config(function ($routeProvider, $httpProvider, uiGmapGoogleMapApiProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'views/main.html',
			controller: 'MainCtrl',
			controllerAs: 'vm'
		})
		.when('/property', {
			templateUrl: 'views/property.html',
			controller: 'PropertyCtrl',
			controllerAs: 'vm'
		})
		.when('/account', {
			templateUrl: 'views/account.html',
			controller: 'AccountCtrl',
			controllerAs: 'vm'
		})
		.when('/buyerProfile', {
			templateUrl: 'views/buyerProfile.html',
			controller: 'BuyerProfileCtrl',
			controllerAs: 'vm'
		})
		.otherwise({
			redirectTo: '/'
		});
		
		$httpProvider.interceptors.push('xmlHttpInterceptor');
		
		uiGmapGoogleMapApiProvider.configure({
			key: 'AIzaSyAP0IUbxIzGKaNdKvafLwbp-iTrwLLND8k',
			libraries: 'geometry'
		});
	});