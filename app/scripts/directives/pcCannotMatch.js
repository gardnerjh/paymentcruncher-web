'use strict';

function pcCannotMatch() {
	return {
		require: 'ngModel',
		scope: {
			otherModelValue: '=pcCannotMatch',
			validateCondition: '=ngRequired'
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.pcCannotMatch = function(modelValue) {
				if (scope.validateCondition) {
					return modelValue !== scope.otherModelValue;
				} else {
					return true;
				}
			};

			scope.$watchCollection('[otherModelValue, validateCondition]', function() {
				ngModel.$validate();
			});
		}
	};
}

angular
	.module('paymentCruncher')
	.directive('pcCannotMatch', pcCannotMatch);