'use strict';

function pcUsername($q, paymentCruncherService) {
	return {
		require: 'ngModel',
		scope: {
			validateCondition: '=pcUsername'
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$asyncValidators.pcUsername = function(modelValue) {
				if (ngModel.$isEmpty(modelValue)) {
					// consider empty model valid
					return $q.when();
				} else if (attributes.pcUsername.length > 0 && !scope.validateCondition) {
					return $q.when();
				}

				var def = $q.defer();

				paymentCruncherService.checkEmailAddress(modelValue)
					.success(function(data) {
						if (data.userExists) {
							def.reject();
						} else {
							def.resolve();
						}
					})
					.error(function(data, status) {
						def.reject();
						alert('Email check was not successful for the following reason: ' + status);
				});

				return def.promise;
			};
			
			scope.$watch('validateCondition', function() {
				ngModel.$validate();
			});
		}
	};
}

angular
	.module('paymentCruncher')
	.directive('pcUsername', ['$q', 'paymentCruncherService', pcUsername]);