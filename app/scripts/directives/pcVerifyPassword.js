'use strict';

function pcVerifyPassword($q, paymentCruncherService) {
	return {
		require: 'ngModel',
		scope: {
			validateCondition: '=ngRequired'
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$asyncValidators.pcVerifyPassword = function(modelValue) {
				if (ngModel.$isEmpty(modelValue) || !scope.validateCondition) {
					// consider empty model valid
					return $q.when();
				}

				var def = $q.defer();

				paymentCruncherService.verifyPassword(modelValue)
					.success(function(data) {
						if (data.passwordMatch) {
							def.resolve();
						} else {
							def.reject();
						}
					})
					.error(function(data, status) {
						def.reject();
						alert('Password check was not successful for the following reason: ' + status);
				});

				return def.promise;
			};
		}
	};
}

angular
	.module('paymentCruncher')
	.directive('pcVerifyPassword', ['$q', 'paymentCruncherService', pcVerifyPassword]);