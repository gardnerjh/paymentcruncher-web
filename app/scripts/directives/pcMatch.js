'use strict';

function pcMatch() {
	return {
		require: 'ngModel',
		scope: {
			otherModelValue: '=pcMatch',
			validateCondition: '=ngRequired'
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.pcMatch = function(modelValue) {
				if (scope.validateCondition) {
					return modelValue === scope.otherModelValue;
				} else {
					return true;
				}
			};

			scope.$watchCollection('[otherModelValue, validateCondition]', function() {
				ngModel.$validate();
			});
		}
	};
}

angular
	.module('paymentCruncher')
	.directive('pcMatch', pcMatch);