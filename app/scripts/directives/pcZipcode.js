'use strict';

function pcZipcode() {
    return {
        require: 'ngModel',
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.pcZipcode = function(modelValue) {
                if (modelValue !== undefined) {
                    if (modelValue.length > 0 && (isNaN(parseFloat(modelValue)) || !isFinite(modelValue) || modelValue.length !== 5)) {
                        return false;
                    }
                }
                return true;
			};
        }
    };
}

angular
	.module('paymentCruncher')
	.directive('pcZipcode', pcZipcode);