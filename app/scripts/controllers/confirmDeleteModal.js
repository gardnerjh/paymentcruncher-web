'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:ConfirmDeleteModalCtrl
 * @description
 * # ConfirmDeleteModalCtrl
 * Controller for the confirm delete modal popup window.
 */
function ConfirmDeleteModalCtrl($modalInstance, recordType, recordName) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.recordType = recordType;
	vm.recordName = recordName;
	
	vm.confirm = function() {
		$modalInstance.close(true);
	};
	
	vm.cancel = function() {
		$modalInstance.close(false);
	};
}

angular
	.module('paymentCruncher')
	.controller('ConfirmDeleteModalCtrl', ['$modalInstance', 'recordType', 'recordName', ConfirmDeleteModalCtrl]);