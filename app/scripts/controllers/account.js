'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:AccountCtrl
 * @description
 * # AccountCtrl
 * Controller for the My Account screen.
 */
function AccountCtrl(paymentCruncherService, loginService) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.loading = true;
	loginService.getLoggedInUser(function(user) {
		if (user) {
			vm.user = user;
		} else {
			alert('Could not get user.');
		}
		vm.loading = false;
	});
	
	vm.saveUser = function(user) {
		vm.loading = true;
		var userToSave = {
			name: user.name,
			email: user.email
		};
		if (user.newPassword) {
			userToSave.password = user.newPassword;
		}
		paymentCruncherService.saveUser(userToSave).then(
			(function() {
				vm.oldPassword = '';
				vm.user.newPassword = '';
				vm.confirmPassword = '';
				
				vm.accountForm.$setPristine();
				vm.accountForm.$setUntouched();
				
				vm.saveSuccessful = true;
				vm.loading = false;
			}),
			(function(response) {
				vm.loading = false;
				alert('Unknown error occurred: ' + response.status);
			})
		);
	};
}

angular
	.module('paymentCruncher')
	.controller('AccountCtrl', ['paymentCruncherService', 'loginService', AccountCtrl]);