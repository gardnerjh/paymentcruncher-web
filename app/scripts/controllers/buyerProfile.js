'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:BuyerProfileCtrl
 * @description
 * # BuyerProfileCtrl
 * Controller for the Buyer Profile screen.
 */
function BuyerProfileCtrl($scope, paymentCruncherService) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.loading = true;
	paymentCruncherService.getBuyerProfiles().then(
		(function(response) {
			vm.buyerProfiles = response.data;
			
			var activeBuyerProfileIndex = -1;
			for (var i = 0; i < vm.buyerProfiles.length; i++) {
				if (vm.buyerProfiles[i].active === true) {
					activeBuyerProfileIndex = i;
					break;
				}
			}
			vm.selectedBuyerProfileIndex = activeBuyerProfileIndex;
			vm.loading = false;
		}),
		(function(response) {
			vm.loading = false;
			alert('Could not get buyer profiles: ' + response.status);
		})
	);
	
	vm.saveBuyerProfile = function(buyerProfile) {
		vm.loading = true;
		var isNewProfile = buyerProfile._id === undefined;
		
		paymentCruncherService.saveBuyerProfile(buyerProfile).then(
			(function(response) {
				vm.buyerProfileForm.$setPristine();
				vm.buyerProfileForm.$setUntouched();
				
				vm.buyerProfile = response.data;
				if (isNewProfile) {
					vm.buyerProfiles.push(response.data);
				}
				
				vm.saveSuccessful = true;
				vm.loading = false;
			}),
			(function(response) {
				vm.saveUnsuccessful = true;
				vm.saveErrorMessage = response.status + ': ' + response.statusText;
				vm.loading = false;
			})
		);
	};
	
	
	$scope.$watch('vm.selectedBuyerProfileIndex', function(newValue, oldValue) {
		 if (newValue !== oldValue) {
			vm.clearOutBuyerProfileForm();
			if (newValue > -1) {
				vm.buyerProfile = vm.buyerProfiles[newValue];
			}
		 }
	 });
	 
	 vm.clearOutBuyerProfileForm = function() {
		vm.saveSuccessful = false;
		vm.saveUnsuccessful = false;
		vm.buyerProfile = {};
		vm.buyerProfileForm.$setPristine();
		vm.buyerProfileForm.$setUntouched();
	};
}

angular
	.module('paymentCruncher')
	.controller('BuyerProfileCtrl', ['$scope', 'paymentCruncherService', BuyerProfileCtrl]);