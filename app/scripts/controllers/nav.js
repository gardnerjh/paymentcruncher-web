'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller for the navigation menu.
 */
function NavCtrl($scope, $location, $cookies, $modal, paymentCruncherService, sharedSettingsService, loginService) {
	/* jshint validthis: true */
	var vm = this;
	
	loginService.getLoggedInUser(function(loggedInUser) {
		if (!loggedInUser) {
			$location.path('/');
		} else {
			vm.loggedInUser = loggedInUser;
		}
	});
	
	$scope.$on('loggedInUser:updated', function(event, data) {
		vm.loggedInUser = data;
	});
	
	vm.openLoginModal = function() {
		$modal.open({
			templateUrl: '/views/loginModal.html',
			controller: 'LoginModalCtrl',
			controllerAs: 'vm'
		});
	};
	
	vm.openLogoutModal = function() {
		$modal.open({
			templateUrl: '/views/logoutModal.html',
			controller: 'LogoutModalCtrl',
			controllerAs: 'vm'
		});
	};
}

angular
	.module('paymentCruncher')
	.controller('NavCtrl', ['$scope', '$location', '$cookies', '$uibModal', 'paymentCruncherService', 'sharedSettingsService', 'loginService', NavCtrl]);