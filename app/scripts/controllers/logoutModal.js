'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:LogoutModalCtrl
 * @description
 * # LogoutModalCtrl
 * Controller for the logout modal popup window.
 */
function LogoutModalCtrl($modalInstance, $location, $cookies, paymentCruncherService, sharedSettingsService, loginService) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.logout = function() {
		vm.loading = true;
		paymentCruncherService.logout().then(
			(function() {
				$cookies.remove(sharedSettingsService.tokenCookieKey);
				loginService.setLoggedInUser();
				$modalInstance.close();
				$location.path('/');
				vm.loading = false;
			}),
			(function() {
				vm.loading = false;
				alert('Log out was not successful.');
			})
		);
	};
}

angular
	.module('paymentCruncher')
	.controller('LogoutModalCtrl', ['$modalInstance', '$location', '$cookies', 'paymentCruncherService', 'sharedSettingsService', 'loginService', LogoutModalCtrl]);