/* global angular */
'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:PropertyCtrl
 * @description
 * # PropertyCtrl
 * Controller for the Property screen.
 */
function PropertyCtrl($scope, $window, $timeout, $filter, $uibModal, uiGmapGoogleMapApi, sharedSettingsService, paymentCruncherService, uspsService) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.getProperties = function() {
		vm.loading = true;
		paymentCruncherService.getProperties().then(
			(function(response) {
				var properties = $filter('orderBy')(response.data, 'sortOrder');

				paymentCruncherService.getActiveBuyerProfile().then(
					(function(response) {
						vm.buyerProfile = response.data;
						vm.properties = properties;
						
						vm.loading = false;
					}),
					(function(response) {
						alert('Could not get active buyer profile: ' + response.status);
						vm.loading = false;
					}));
			}),
			(function(response) {
				vm.loading = false;
				alert('Could not get properties: ' + response.status);
			})
		);
	};
	
	vm.getProperties();
	
	vm.sortableOptions = {
		cursor: 'move',
		update: function(e, ui) {
			ui.item.parent().sortable('disable');
			var sortOrderArray = ui.item.parent().sortable('toArray');
			paymentCruncherService.updatePropertySortOrder(sortOrderArray).then(
				(function() { 
					ui.item.parent().sortable('enable');
				}),
				(function(response) {
					ui.item.parent().sortable('enable');
					alert('Could not save sort order: ' + response.status);
				})
			);
		}
	};
		
	$scope.$on('loggedIn:updated', function(event, data) {
		if (data) {
			vm.getProperties();
		} else {
			vm.properties = [];
			vm.setSelectedPropertyIndex();
		}
	});
	
	vm.setSelectedPropertyIndex = function(index) {
		if (index === undefined) {
			index = -1;
		}
		vm.selectedPropertyIndex = index;
	};
	
	vm.saveProperty = function(property) {
		vm.loading = true;
		
		property.location = {};
		var addressString = property.address + ', ' + property.city + ', ' + property.state + ' ' + property.zip;
		geocodeAddress(addressString, function(latitude, longitude, error) {
			if (property.location === undefined) {
				property.location = {};
			}
			if (error) {
				alert('Unknown error occurred while geocoding address: ' + error);
				vm.loading = false;
			} else {
				property.location.coordinates = { latitude: latitude, longitude: longitude };
				
				getStreetViewData(latitude, longitude, function(streetViewObject, error) {
					if (!error) {
						property.location.streetView = streetViewObject;
					}
					
					paymentCruncherService.saveProperty(property).then(
						(function(response) {
							vm.property = response.data;
							if (property.location.streetView) {
								setPanorama(streetViewObject);
							}
							
							vm.propertyForm.$setPristine();
							vm.propertyForm.$setUntouched();
							
							if (vm.selectedPropertyIndex === -1) {
								vm.properties.push(response.data);
								vm.selectedPropertyIndex = vm.properties.length - 1;
							} else {
								vm.properties[vm.selectedPropertyIndex] = response.data;
							}
							
							vm.saveSuccessful = true;
							vm.loading = false;
						}),
						(function(response) {
							vm.saveErrorMessage = response.status + ': ' + response.statusText;
							vm.saveUnsuccessful = true;
							vm.loading = false;
						})
					);
				});
			}
		});
	};
	
	vm.standardizeAddress = function(property) {
		vm.loading = true;
		uspsService.getStandardizedAddress(property.address, property.city, property.state).then(
			(function(response) {
				var standardizedAddress = response.data.AddressValidateResponse.Address.Address2;
				var standardizedCity = response.data.AddressValidateResponse.Address.City;
				var standardizedState = response.data.AddressValidateResponse.Address.State;
				
				if (standardizedAddress !== property.address || standardizedCity !== property.city || standardizedState !== property.state) {
					vm.loading = false;
					
					var modalInstance = $uibModal.open({
						templateUrl: '/views/standardizeAddressModal.html',
						controller: 'StandardizeAddressModalCtrl',
						controllerAs: 'vm',
						keyboard: false,
						resolve: {
							standardizedAddress: function() {
								return standardizedAddress;
							},
							standardizedCity: function() {
								return standardizedCity;
							},
							standardizedState: function() {
								return standardizedState;
							}
						}
					});
					modalInstance.result.then(
						function(useStandardizedAddress) {
							if (useStandardizedAddress !== undefined && useStandardizedAddress) {
								vm.property.address = standardizedAddress;
								vm.property.city = standardizedCity;
								vm.property.state = standardizedState;
								vm.saveProperty(vm.property);
							} else if (useStandardizedAddress !== undefined && !useStandardizedAddress) {
								vm.saveProperty(vm.property);								
							}
						}
					);
				} else {
					vm.saveProperty(property);
				}
			}),
			(function(response) {
				vm.saveErrorMessage = response.status + ': ' + response.statusText;
				vm.saveUnsuccessful = true;				
				vm.loading = false;
			})
		);
	};
	
	function setPanorama(streetViewObject) {
		var panoramaId;
		var pointOfView;
		if (streetViewObject !== undefined) {
			panoramaId = streetViewObject.panoramaId;
			pointOfView = streetViewObject.pointOfView;
		}
		
		uiGmapGoogleMapApi.then(function(maps) {
			vm.panorama = new maps.StreetViewPanorama(document.getElementById('propertyStreetView'), {
				disableDefaultUI: true,
				scrollwheel: false,
				pano: panoramaId,
				pov: pointOfView
			});
		});
	}
	
	 $scope.$watch('vm.selectedPropertyIndex', function(newValue, oldValue) {
		 if (newValue !== oldValue) {
			vm.clearOutPropertyForm(false);
			if (newValue > -1) {
				vm.property = vm.properties[newValue];
				setPanorama(vm.properties[newValue].location.streetView);
				if (vm.properties[newValue].purchasePrice > 0) {
					vm.showPurchasePrice = true;
				}
			}
		 }
	 });
	
	$scope.$watch('vm.bigPropertyStreetViewShown', function(newValue, oldValue) {
		if (newValue !== oldValue) {
			uiGmapGoogleMapApi.then(function(maps) {
				if (newValue) {
					// Wait for a fraction of a second to give div time to be visible. Otherwise street view image won't show.
					$timeout(function(){
						var panoramaId;
						var pointOfView;
						if (vm.property.location.streetView !== undefined) {
							panoramaId = vm.property.location.streetView.panoramaId;
							pointOfView = vm.property.location.streetView.pointOfView;
						}
						
						new maps.StreetViewPanorama(document.getElementById('bigPropertyStreetView'), {
							pano: panoramaId,
							pov: pointOfView
						});
					}, 1);
				} else {
					new maps.StreetViewPanorama(document.getElementById('bigPropertyStreetView'));
				}
			});
		}
	});
	
	$scope.$watch('vm.propertyListMapShown', function(newValue, oldValue) {
		if (newValue !== oldValue) {
			if (newValue && vm.properties.length > 0) {
				vm.loading = true;
				uiGmapGoogleMapApi.then(function(maps) {
					var processedCount = 0;
					var successfulCount = 0;
					vm.mapBounds = new maps.LatLngBounds();
					angular.forEach(vm.properties, function(value) {
						if (value.location.coordinates !== undefined) {
							vm.mapBounds.extend(new maps.LatLng(value.location.coordinates.latitude, value.location.coordinates.longitude));
							successfulCount++;
						}
						
						processedCount++;
						if (processedCount === vm.properties.length) {
							$timeout(function() {
								var mapHeight = angular.element(document.getElementById('propertyListMap').getElementsByClassName('angular-google-map-container')).css('height').replace('px', '');
								var mapWidth = angular.element(document.getElementsByClassName('panel')[0]).width();
								var zoom = getBoundsZoomLevel(vm.mapBounds, {height: mapHeight, width: mapWidth});
								
								vm.map = {
									center: { 
										latitude: vm.mapBounds.getCenter().lat(), 
										longitude: vm.mapBounds.getCenter().lng()
									}, 
									zoom: zoom
								};
								vm.propertyListMapShown = true;
								vm.loading = false;
								
								if (successfulCount !== vm.properties.length) {
									$window.alert('Not all properties were shown.');
								}
							}, 1);
						}
					});
				});
			}
		}
	});
	
	vm.deleteProperty = function(property) {
		var modalInstance = $uibModal.open({
			templateUrl: '/views/confirmDeleteModal.html',
			controller: 'ConfirmDeleteModalCtrl',
			controllerAs: 'vm',
			keyboard: false,
			resolve: {
				recordType: function() {
					return 'property';
				},
				recordName: function() {
					return property.name;
				}
			}
		});
		modalInstance.result.then(
			function(wasConfirmed) {
				if (wasConfirmed) {
					vm.loading = true;
					paymentCruncherService.deleteProperty(property).then(
						(function() {
							if (vm.property !== undefined && property._id === vm.property._id) {
								vm.setSelectedPropertyIndex();
							}
							vm.properties = $filter('filter')(vm.properties, function(value) {return value._id !== property._id;});
							vm.loading = false;
						}),
						(function(response) {
							alert('Unknown error occurred while deleting property: ' + response.status);				
							vm.loading = false;
						})
					);
				}
			}
		);
	};
	
	vm.getAmortizationSchedule = function(property) {
		if (!property.amortizationSchedule) {
			vm.loading = true;
			paymentCruncherService.getAmortizationSchedule(property).then(
				(function(response) {
					property.amortizationSchedule = response.data;
					vm.showAmortizationSchedule = true;
					vm.loading = false;
				}),
				(function(response) {
					alert('Unknown error occurred while getting amortization schedule: ' + response.status);				
					vm.loading = false;
				})
			);
		} else {
			vm.showAmortizationSchedule = true;			
		}
	};
	
	vm.clearOutPropertyForm = function(initializeTabs) {
		vm.saveSuccessful = false;
		vm.saveUnsuccessful = false;
		vm.property = {};
		vm.panorama = {};
		setPanorama();
		vm.propertyForm.$setPristine();
		vm.propertyForm.$setUntouched();
		vm.showPurchasePrice = false;
		vm.showMiscExpenses = false;
		if (initializeTabs) {
			vm.initializeTabs = true;
		}
	};
	
	function geocodeAddress(address, callback) {
		uiGmapGoogleMapApi.then(function(maps) {
			var geocoder = new maps.Geocoder();
			geocoder.geocode({ 'address': address }, function(results, status) {
				if (status === maps.GeocoderStatus.OK) {
					if (results[0]) {
						callback(results[0].geometry.location.lat(), results[0].geometry.location.lng());
					} else {
						callback();
					}
				} else {
					callback(null, null, status);
				}
			});
		});
	}
	
	function getStreetViewData(latitude, longitude, callback) {
		uiGmapGoogleMapApi.then(function(maps) {
			var sv = new maps.StreetViewService();
			var propertyLatLng = new maps.LatLng(latitude, longitude);
			sv.getPanorama({ location: propertyLatLng }, function(data, status) {
				if (status === maps.StreetViewStatus.OK) {
					var heading = maps.geometry.spherical.computeHeading(data.location.latLng, propertyLatLng);
					var streetView = {
						panoramaId: data.location.pano,
						pointOfView: {
							heading: heading,
							pitch: 0,
							zoom: 1
						}
					};
					callback(streetView);
				} else {
					callback(null, status);
				}
			});
		});
	}
	
	function getBoundsZoomLevel(bounds, mapDim) {
		var WORLD_DIM = { height: 256, width: 256 };
		var ZOOM_MAX = 21;
	
		function latRad(lat) {
			var sin = Math.sin(lat * Math.PI / 180);
			var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
			return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
		}
	
		function zoom(mapPx, worldPx, fraction) {
			return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
		}
	
		var ne = bounds.getNorthEast();
		var sw = bounds.getSouthWest();
	
		var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;
	
		var lngDiff = ne.lng() - sw.lng();
		var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;
	
		var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
		var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);
	
		return Math.min(latZoom, lngZoom, ZOOM_MAX);
	}
}

angular
	.module('paymentCruncher')
	.controller('PropertyCtrl', ['$scope', '$window', '$timeout', '$filter', '$uibModal', 'uiGmapGoogleMapApi', 'sharedSettingsService', 'paymentCruncherService', 'uspsService', PropertyCtrl]);