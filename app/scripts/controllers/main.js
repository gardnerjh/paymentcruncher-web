'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the paymentCruncher
 */
function MainCtrl($scope, $modal, loginService) {
  var vm = this;
  
  loginService.getLoggedInUser(function(user) {
    if (user !== null) {
      vm.loggedInUser = user;
    }
  });
  
  vm.openLoginModal = function() {
    $modal.open({
      templateUrl: '/views/loginModal.html',
      controller: 'LoginModalCtrl',
      controllerAs: 'vm'
    });
  };
  
  $scope.$on('loggedInUser:updated', function(event, data) {
		vm.loggedInUser = data;
	});
}

angular
  .module('paymentCruncher')
  .controller('MainCtrl', ['$scope', '$modal', 'loginService', MainCtrl]);