'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:LoginModalCtrl
 * @description
 * # LoginModalCtrl
 * Controller for the login modal popup window.
 */
function LoginModalCtrl($scope, $modalInstance, $cookies, paymentCruncherService, sharedSettingsService, loginService) {
	/* jshint validthis: true */
	var vm = this;
	
	vm.login = function(credentials) {
		vm.loading = true;
		paymentCruncherService.login({email: credentials.email, password: credentials.password}, vm.loginType).then(
			(function(response) {
				vm.credentials = {};
				vm.loginForm.$setPristine();
				vm.loginForm.$setUntouched();
				$modalInstance.close();
				
				var cookieOptions;
				if (credentials.rememberMe) {
					var now = new Date();
					var expires = new Date(now.setDate(now.getDate() + sharedSettingsService.rememberMeExpirationDays));
					cookieOptions = {
						expires: expires
					};
				}
				$cookies.put(sharedSettingsService.tokenCookieKey, response.data.rawToken, cookieOptions);
				loginService.setLoggedInUser(response.data.user);
				vm.loading = false;
			}),
			(function(response) {
				var errorMessage;
				if (response.status === 401) {
					errorMessage = 'Check your username and password and try again.';
				} else {
					errorMessage = 'Unknown error occurred.';
				}
				vm.loading = false;
				alert(errorMessage);
			})
		);
	};
}

angular
	.module('paymentCruncher')
	.controller('LoginModalCtrl', ['$scope', '$modalInstance', '$cookies', 'paymentCruncherService', 'sharedSettingsService', 'loginService', LoginModalCtrl]);