'use strict';

/**
 * @ngdoc function
 * @name paymentCruncher.controller:StandardizeAddressModalCtrl
 * @description
 * # StandardizeAddressModalCtrl
 * Controller for the standardize address modal on the Property screen.
 */
function StandardizeAddressModalCtrl($modalInstance, standardizedAddress, standardizedCity, standardizedState) {
        var vm = this;
        
        if (standardizedAddress !== undefined && standardizedCity !== undefined && standardizedState !== undefined) {
                vm.isValidAddress = true;
                vm.standardizedAddress = standardizedAddress;
                vm.standardizedCity = standardizedCity;
                vm.standardizedState = standardizedState;
        } else {
                vm.isValidAddress = false;
        }

        vm.useOriginalAddress = function () {                    
                $modalInstance.close(false);
        };
        
        vm.useStandardizedAddress = function () {                    
                $modalInstance.close(true);
        };
        
        vm.closeModal = function () {                    
                $modalInstance.close();
        };
}

angular
        .module('paymentCruncher')
        .controller('StandardizeAddressModalCtrl', ['$modalInstance', 'standardizedAddress', 'standardizedCity', 'standardizedState', StandardizeAddressModalCtrl]);