'use strict';

function loginService($rootScope, $cookies, paymentCruncherService, sharedSettingsService) {
	var service = {};
	
	var loggedInUser;
	service.getLoggedInUser = function(callback) { 
		if (loggedInUser === undefined && $cookies.get(sharedSettingsService.tokenCookieKey) !== undefined) {
			paymentCruncherService.getUser().then(
				(function(result) {
					loggedInUser = result.data;
					callback(loggedInUser);
				}),
				(function(){
					callback(null);
				})
			);
		} else {
			callback(loggedInUser);
		}
	};
	
	service.setLoggedInUser = function(user) {
		service.loggedInUser = user;
		$rootScope.$broadcast('loggedInUser:updated', service.loggedInUser);
	};
	
	return service;
}

angular
	.module('paymentCruncher')
	.factory('loginService', ['$rootScope', '$cookies', 'paymentCruncherService', 'sharedSettingsService', loginService]);