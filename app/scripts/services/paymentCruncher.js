'use strict';

function paymentCruncherService($http, $cookies, sharedSettingsService) {
	// Factory object to be returned by this function
	var service = {};
	
	service.login = function(user, loginType) {
		var url = sharedSettingsService.paymentCruncherApiBaseUrl;
		if (loginType === 'signup') {
			url += 'user';
		} else {
			url += 'login';
		}
		
		return $http({
			method: 'POST',
			url: url,
			data: user,
			headers: {'Content-Type': 'application/json'}
		});
	};
	
	service.logout = function() {
		return $http({
			method: 'POST',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'logout',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.checkEmailAddress = function(emailAddress) {
		return $http.get(sharedSettingsService.paymentCruncherApiBaseUrl + 'user/validateEmail/' + emailAddress);
	};
	
	service.verifyPassword = function(password) {
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'user/verifyPassword?password=' + password,
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.getUser = function() {
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'user',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.saveUser = function(user) {
		var postData = {
			name: user.name,
			email: user.email
		};
		if (user.password) {
			postData.password = user.password;
		}
		
		return $http({
			method: 'PUT',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'user',
			data: postData,
			headers: {'Content-Type': 'application/json', 'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.getActiveBuyerProfile = function() {
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'user/buyerProfile/active',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.getBuyerProfiles = function() {
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'buyerProfile',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.saveBuyerProfile = function(buyerProfile) {
		var url = sharedSettingsService.paymentCruncherApiBaseUrl + 'buyerProfile';
		var method = 'POST';
		if (buyerProfile._id) {
			url += '/' + buyerProfile._id;
			method = 'PUT';
		}
		
		return $http({
			method: method,
			url: url,
			data: buyerProfile,
			headers: {'Content-Type': 'application/json', 'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};

	service.getProperties = function() {
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'property',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.saveProperty = function(property) {
		var url = sharedSettingsService.paymentCruncherApiBaseUrl + 'property';
		var method = 'POST';
		if (property._id) {
			url += '/' + property._id;
			method = 'PUT';
		}
		
		return $http({
			method: method,
			url: url,
			data: property,
			headers: {'Content-Type': 'application/json', 'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.updatePropertySortOrder = function(sortOrderArray) {
		var payload = {
			sortOrder: sortOrderArray
		};
		
		return $http({
			method: 'PUT',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'property/sortOrder',
			data: payload,
			headers: {'Content-Type': 'application/json', 'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};

	service.deleteProperty = function(property) {
		var propertyId = property._id;
		
		return $http({
			method: 'DELETE',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'property/' + propertyId,
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	service.getAmortizationSchedule = function(property) {
		var propertyId = property._id;
		
		return $http({
			method: 'GET',
			url: sharedSettingsService.paymentCruncherApiBaseUrl + 'property/' + propertyId + '/amortizationSchedule',
			headers: {'Auth-Token': $cookies.get(sharedSettingsService.tokenCookieKey)}
		});
	};
	
	return service;
}

angular
	.module('paymentCruncher')
	.factory('paymentCruncherService', ['$http', '$cookies', 'sharedSettingsService', paymentCruncherService]);