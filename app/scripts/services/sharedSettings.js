'use strict';

function sharedSettingsService() {
	var service = {};
	
	service.paymentCruncherApiBaseUrl = 'http://localhost:3000/';
	service.uspsValidateApiBaseUrl = 'https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML=';
	service.uspsValidateXmlTemplate = '<AddressValidateRequest USERID="835JEFFG1114"><Address ID="0"><FirmName></FirmName><Address1></Address1><Address2>##ADDRESS##</Address2><City>##CITY##</City><State>##STATE##</State><Zip5></Zip5><Zip4></Zip4></Address></AddressValidateRequest>';
	service.tokenCookieKey = 'paymentCruncherToken';
	service.rememberMeExpirationDays = 7;
	
	return service;
}

angular
	.module('paymentCruncher')
	.factory('sharedSettingsService', sharedSettingsService);