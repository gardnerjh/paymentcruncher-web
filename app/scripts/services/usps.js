'use strict';

function uspsService($http, sharedSettingsService) {
	// Factory object to be returned by this function
	var service = {};
	
	service.getStandardizedAddress = function(address, city, state) {
		var xml = sharedSettingsService.uspsValidateXmlTemplate.replace("##ADDRESS##", address).replace("##CITY##", city).replace("##STATE##", state);		
		return $http.get(sharedSettingsService.uspsValidateApiBaseUrl + xml);
	};
	
	return service;
}

angular
	.module('paymentCruncher')
	.factory('uspsService', ['$http', 'sharedSettingsService', uspsService]);